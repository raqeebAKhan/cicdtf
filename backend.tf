terraform {
  backend "s3" {
    bucket         = "s3statebucket15"
    key            = "terraform.tfstate"
    region         = "us-east-1"
    dynamodb_table = "terraform_state_lock"
  }
}




